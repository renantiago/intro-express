import express from 'express';

export default function projectsRouter (db)  {
  const router = express.Router();
  router.post('/', async (req, res) => {
      const { name } = req.body;
  
      const slug = name.toLowerCase();
  
      const project = {
        slug,
        boards: [],
      };
  
      const existingProject = await db.collection('projects').findOne({ slug });
  
      if (existingProject) {
        res.status(400).json({ error: `project ${slug} already exists` });
      } else {
        await db.collection('projects').insertOne(project);
        res.json({ project });
      }
    });
  
  const findProject = async (req, res, next) => {
      const { slug } = req.params;
  
      const project = await db.collection('projects').findOne({ slug });
  
      if (!project) {
        res.status(400).json({ error: `project ${slug} does not exist` });
      } else {
        req.body.project = project;
        next();
      }
  };

  const checkBoard = async (req, res, next) => {
    const { project } = req.body;
    const { name } = req.params;
    const index = project.boards.findIndex(item => item.name == name);
    if (index == -1) {
      res.status(400).json({ error: `board ${name} does not exist` });
      return;
    }
    req.body.boardIndex = index;
    next();
  }
  
  router.get('/:slug', findProject, async (req, res) => {
      const { project } = req.body;
      res.json({ project });
  });
  
  router.post('/:slug/boards', findProject, async (req, res) => {
      const { name, project } = req.body;
      const board = { name, tasks: [] };
      project.boards.push(board);
  
      await db
        .collection('projects')
        .findOneAndReplace({ slug: project.slug }, project);
  
      res.json({ board });
  });

  router.delete('/:slug', findProject, async (req, res) => {
    const { name, project } = req.body;

    await db
      .collection('projects')
      .deleteOne({ slug: project.slug });

    res.status(204).send();
  });

  router.delete('/:slug/boards/:name', findProject, checkBoard, async (req, res) => {
    const { project, boardIndex } = req.body;
    project.boards.splice(boardIndex, 1);
    await db
      .collection('projects')
      .findOneAndReplace({ slug: project.slug }, project);
    
    res.status(204).send();
  });

  router.post('/:slug/boards/:name/tasks', findProject, checkBoard, async (req, res) => {
    const { text } = req.body;
    const { name } = req.params;

    const task = {text, boardName: name};
    
    await db
      .collection('tasks')
      .insertOne(task);
    
    res.json({task});
  });

  router.delete('/:slug/boards/:name/tasks/:id', findProject, checkBoard, async (req, res) => {
    const { name, id } = req.params;

    await db
      .collection('tasks')
      .deleteOne({id, boardName: name});
    
    res.status(204).send();

  });

  return router; 
}


