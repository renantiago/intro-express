import request from 'supertest';

import { app, bootstrap } from '../src/app';

describe('our app', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  // código pt. 1
  test('GET / returns a "hello world"', async () => {
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });

  // código pt. 2
  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt.2
  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 3
  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  test('DELETE /projects/:slug delete existing project', async () => {
    const project = { name: 'bagulho' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const { status } = await request(app).delete(`/projects/${slug}`);
    
    expect(status).toBe(204);
  });

  test('DELETE /projects/:slug should not delete unknown project', async () => {
    const slug = 'unknown-project';
    
    const res = await request(app).delete(`/projects/${slug}`);
    
    expect(res.body.error).toBe(`project ${slug} does not exist`)
    expect(res.status).toBe(400);
  });

  test('DELETE /projects/:slug/boards/:name should delete board', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body: {
        board
      }
    } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    const { status } = await request(app).delete(`/projects/${slug}/boards/${name}`);

    expect(status).toBe(204);
  });

  test('DELETE /projects/:slug/boards/:name should not delete unknown board', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const res = await request(app).delete(`/projects/${slug}/boards/${name}`);

    expect(res.body.error).toBe(`board ${name} does not exist`)
    expect(res.status).toBe(400);
  });

  test('DELETE /projects/:slug/boards/:name should not delete board from unknown project', async () => {
    const name = 'todo';

    const res = await request(app).delete(`/projects/unknown/boards/${name}`);

    expect(res.body.error).toBe(`project unknown does not exist`)
    expect(res.status).toBe(400);
  });

  test('POST /projects/:slug/boards/:name should create task', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body: {
        board
      }
    } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });
    
    

    const { body } = await request(app).post(`/projects/${slug}/boards/${name}/tasks`)
      .send({ text: "vixe" });

    expect(body.task.text).toEqual("vixe");
   });

   test('DELETE /projects/:slug/boards/:name/tasks/:id should delete task', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body: {
        board
      }
    } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });
    
    

    const { body: { task } } = await request(app)
      .post(`/projects/${slug}/boards/${name}/tasks`);

    const res = await request(app).delete(`/projects/${slug}/boards/${name}/tasks/${task.id}`)


    expect(res.status).toEqual(204);
   });

});
